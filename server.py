#SERVER
import socket
from tictactoe import *

class Server:
    def __init__(self):
        self.game = old()
        self.addresses = []
        self.miscdata = []
        self.turns = {'X': '', 'O':''}
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind(('', 6789))
        #Timeout time for connection losses
        self.sock.settimeout(60)


    def sendBoth(self, data):
        '''
        Sends data to players
        '''

        self.sock.sendto((data.encode()), (self.addresses[0]))



    def send(self, data, addr):

        self.sock.sendto((data.encode()), addr)


    def sendTable(self):
        '''
        Converts to string and prints the board
        '''

        data = (', '.join((str(x) for x in self.game.table)))
        self.sendBoth(data)


    def wait(self):
        '''
        Waits for a move and saves address
        '''
        while True:
            data, addr = self.sock.recvfrom(1024)
            (self.addresses).append(addr)
            if addr:
                break
        return data


    def start(self):

        print ("Starting server...")
        print ("Waiting for connections...\n")

        while len(self.addresses) == 0 :
            #Waiting for a connections
            name = self.wait()
            self.miscdata.append(name.decode())
        

        self.turns['X'] = self.addresses[0]

        self.send("ok", self.turns['X'])
        clientStart = self.wait()
        self.sendTable()

        if (clientStart == b'True'):
            self.game.turn_current = 0
            self.game.clientStart = True
        else:
            self.game.clientStart = False
            self.game.turn_current = 1
        #Loop for game
        while not self.game.over():
            try:
                if self.game.turn_current % 2 == 0:
                    #Player X's turn
                    self.send('Your turn.', self.turns['X'])

                    move, addr = self.sock.recvfrom(1024)
                    self.game.move(move.decode())
                    self.sendTable()

                else:
                    #Computers's turn
                    self.send('Computer\'s turn', self.turns['X'])              
                    self.game.compMove()
                    self.sendTable()

            except ConnectionResetError:
                #One of the players has disconnected
                self.sendBoth('999')
                print ("Connection Error")

        winner = self.game.winner

        if winner == 'X':
            #X wins
            self.sendBoth('101')
        elif winner == 'O':
            #O wins
            self.sendBoth('201')
        else:
            #Tie
            self.sendBoth('000')


if __name__ == '__main__':
    try:
        a = Server()
        a.start()

    except KeyboardInterrupt:
        print("\nDetected keyboard interrupt... bye!")

    except socket.timeout:
        print ("Time out - socket closed")

