class old():
    def __init__(self):
        self.table = ['-'] + list(range(1, 10))
        self.players = ['X', 'O']
        self.winner = None
        self.turn_current = 0
        self.winningCombos = [
           (1, 2, 3),
           (4, 5, 6),
           (7, 8, 9),
           (1, 4, 7),
           (2, 5, 8),
           (3, 6, 9),
           (1, 5, 9),
           (3, 5, 7),
        ]
        self.clientStart = True

    @staticmethod
    def draw(tableList):
        print ()
        print ('', tableList[1], tableList[2], tableList[3], '\n', 
        tableList[4], tableList[5], tableList[6], '\n',
        tableList[7], tableList[8], tableList[9])
        print ()
        

    def turn(self):
        if self.turn_current%2 == 0:
            return 'X'
        else:
            return 'O'

    def move(self, home):
        try:
            if int(home) in self.table:
                self.table[int(home)] = 'X'
                self.turn_current += 1
                #move is valid
                #table updated
                return True
            else:
                #move invalid
                return False
        except ValueError:
            #value is invalid
            return False

    def compMove(self):
        for i in range(1, len(self.table)):
            if self.table[i] != 'X' and self.table[i] != 'O':
                self.table[i] = 'O'
                self.turn_current += 1
                break

    def over(self):
        for a, b, c in self.winningCombos:
            if self.table[a] == self.table[b] == self.table[c]:
                self.turn_current += 1
                self.winner = self.turn()
                
                return True
            
        if self.turn_current == 9 and self.clientStart:
            print("Tie\n")
            return True
        
        elif self.turn_current == 10 and not self.clientStart:
            print("Tie\n")
            return True
        else:
            #Game not over
            return False

        

