#CLIENT
import socket
from tictactoe import *

from argparse import ArgumentParser



class Client:

    def __init__(self):
        self.ip = serverIP
        self.port = "6789"
        self.address = (serverIP, int(self.port))
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.settimeout(60)

    def move(self):
        data = input("Make your move: ")
        self.sock.sendto(data.encode(), (self.address))

    def recieve(self):
        data, addr = self.sock.recvfrom(1024)
        return (data)

    def buildTable(self, tabstr):
        '''
        Converts to string and prints the board
        '''

        if len(tabstr) == 28:
            tableList = tabstr.split(",")
            old.draw(tableList)
        else:
            print (tabstr)

    def start(self):

        print ("Connecting to server...")

        player = str(input("Enter Player name: "))
        self.sock.sendto(player.encode(), (self.address))

        # get ok from server
        data = self.recieve()

        self.sock.sendto(str(clientStart).encode(), self.address)

        # get table from server
        data = self.recieve()

        self.buildTable(data.decode())

        while True:

            order = self.recieve()

            if order:
                if order.decode() == '000':
                    print ("It's a draw!")
                    break

                elif order.decode() == '101':
                    print ("Player X wins!")
                    break

                elif order.decode() == '201':
                    print ("Computer wins!")
                    break

                elif order.decode() == '999':
                    print ("Connection lost with one of the players.")
                    break

            self.buildTable(order.decode())

            if str(order.decode()) == "Computer's turn.":
                #Wait
                tab = self.recieve()
                self.buildTable(tab.decode())

            if str(order.decode()) == "Your turn.":
                #Play
                self.move()
                tab = self.recieve()
                self.buildTable(tab.decode())


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument("-s", "--serverIP", dest="serverIP", required=True)
    parser.add_argument("-c", "--clientStart", dest="clientStart", action="store_true", default=False)

    args = parser.parse_args()

    serverIP = args.serverIP
    clientStart = args.clientStart

    try:
        client = Client()
        client.start()

    except KeyboardInterrupt:
        print ("\nDetected keyboard interrupt... bye!")
    except socket.timeout:
        print ("Time out - socket closed")
